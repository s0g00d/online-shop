package com.online.shop.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Product name already exist!")
public class ProductNameAlreadyExistsException extends RuntimeException {
    private int statusCode;

    public ProductNameAlreadyExistsException() {
        this.statusCode = 404;
    }

    public ProductNameAlreadyExistsException(String message) {
        super(message);
        this.statusCode = 404;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
