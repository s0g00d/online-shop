package com.online.shop.web.controllers;

import com.online.shop.domain.enitities.ShoppingCart;
import com.online.shop.domain.model.rest.CartBean;
import com.online.shop.domain.model.service.*;
import com.online.shop.domain.model.view.CartViewModel;
import com.online.shop.service.CartService;
import com.online.shop.service.OrderService;
import com.online.shop.service.ProductService;
import com.online.shop.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/cart")
//@CrossOrigin(origins = "*")
public class CartController extends BaseController {

    private final CartApiController cartApi;
    private final ModelMapper modelMapper;
    private final ProductService productService;
    private final CartService cartService;
    private final UserService userService;
    private final OrderService orderService;

    @Autowired
    public CartController(CartApiController cartApi, ModelMapper modelMapper, ProductService productService, CartService cartService, UserService userService, OrderService orderService) {
        this.cartApi = cartApi;
        this.modelMapper = modelMapper;
        this.productService = productService;
        this.cartService = cartService;
        this.userService = userService;
        this.orderService = orderService;
    }

    @GetMapping("/details")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView showCart(ModelAndView modelAndView, HttpSession session, Principal principal) { //, @ModelAttribute(name = "model") CartBindingModel model , List<CartBean> cartBean
        var cart = this.retrieveCart(session);

        List<CartBean> shopItems = (List<CartBean>) session.getAttribute("shopping-cart");
        BigDecimal calcTotal = new BigDecimal(0);

        if (principal.getName() != null) {

            List<CartServiceModel> itemsDB = this.cartService.listAllCartItems(principal.getName());
            if (itemsDB.isEmpty()) {
                this.cartService.addItemsToCart(shopItems, principal.getName());
            }
            List<CartViewModel> modelDB = this.cartService.listAllCartItems(principal.getName())
                    .stream()
                    .map(sc -> this.modelMapper.map(sc, CartViewModel.class))
                    .collect(Collectors.toList());

            calcTotal = populateShoppingCartAndReturnTotalAmount(modelDB);
            modelAndView.addObject("totalPrice", calcTotal);
            modelAndView.addObject("items", modelDB);

        } else {
            List<CartViewModel> model = shopItems.stream()
                    .map(items -> this.modelMapper.map(items, CartViewModel.class))
                    .collect(Collectors.toList());

            calcTotal = populateShoppingCartAndReturnTotalAmount(model);
            modelAndView.addObject("totalPrice", calcTotal);
            modelAndView.addObject("items", model);
        }

        return super.view("/cart-details", modelAndView);
    }

    @PostMapping("/remove-product")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView removeFromCartConfirm(String name, HttpSession session, Principal principal) {
        this.removeItemFromCart(name, this.retrieveCart(session), principal.getName());
        return super.redirect("/cart/details");
    }

    @PostMapping("/checkout")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView checkoutConfirm(HttpSession session, Principal principal) {
        var cart = this.retrieveCart(session);

        OrderServiceModel orderServiceModel = this.prepareOrder(cart, principal.getName());
        this.orderService.createOrder(orderServiceModel);
        return super.redirect("/home");
    }

    private OrderServiceModel prepareOrder(List<CartBean> cart, String customer) {
        OrderServiceModel orderServiceModel = new OrderServiceModel();
        orderServiceModel.setCustomer(this.userService.findUserByUserName(customer));
        List<ProductServiceModel> products = new ArrayList<>();
        for (CartBean item : cart) {
            ProductServiceModel productServiceModel = this.modelMapper.map(this.productService.findProductByName(item.getName()), ProductServiceModel.class);
            ProductServiceModel orderProductServiceModel = this.modelMapper.map(productServiceModel, ProductServiceModel.class);

            for (int i = 0; i < item.getQuantity(); i++) {
                products.add(orderProductServiceModel);
            }
        }
        List<CartViewModel> modelDB = this.cartService.listAllCartItems(customer)
                .stream()
                .map(sc -> this.modelMapper.map(sc, CartViewModel.class))
                .collect(Collectors.toList());
        orderServiceModel.setProducts(products);
        orderServiceModel.setTotalPrice(this.populateShoppingCartAndReturnTotalAmount(modelDB));

        return orderServiceModel;
    }

    private void removeItemFromCart(String name, List<CartBean> cart, String userName) {
        UserServiceModel user = this.userService.findUserByUserName(userName);
        CartServiceModel cartServiceModel = this.cartService.findUserItemByName(name, user.getId());
        this.cartService.removeItemFromCart(this.modelMapper.map(cartServiceModel, ShoppingCart.class).getId());
        cart.removeIf(ci -> ci.getName().equals(name));
    }

    private boolean checkSameObjects(List<CartBean> shopItems, List<CartServiceModel> shopItemsDB) {

        List<CartServiceModel> shopItemsSession = shopItems.stream()
                .map(items -> this.modelMapper.map(items, CartServiceModel.class))
                .collect(Collectors.toList());
        if (shopItemsDB.equals(shopItemsSession)) {
            return true;
        } else {
            return false;
        }
    }

    private BigDecimal populateShoppingCartAndReturnTotalAmount(List<CartViewModel> model) {
        BigDecimal calcTotal = new BigDecimal(0);
        for (int i = 0; i < model.size(); i++) {
            ProductServiceModel productServiceModel = this.productService.findProductByName(model.get(i).getName());
            model.get(i).setDescription(productServiceModel.getDescription());
            model.get(i).setImageUrl(productServiceModel.getImageUrl());
            model.get(i).setPrice(productServiceModel.getPrice());
            calcTotal = calcTotal.add(model.get(i).getPrice().multiply(new BigDecimal(model.get(i).getQuantity())));
        }
        return calcTotal;
    }

    private List<CartBean> retrieveCart(HttpSession session) {
        this.initCart(session);

        return (List<CartBean>) session.getAttribute("shopping-cart");
    }

    private void initCart(HttpSession session) {
        if (session.getAttribute("shopping-cart") == null) {
            session.setAttribute("shopping-cart", new LinkedList<>());
        }
    }
}
