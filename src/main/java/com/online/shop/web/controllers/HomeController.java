package com.online.shop.web.controllers;

import com.online.shop.domain.model.view.CategoryAllViewModel;
import com.online.shop.service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class HomeController extends BaseController {

    private final ModelMapper modelMapper;
    private final CategoryService categoryService;

    @Autowired
    public HomeController(ModelMapper modelMapper, CategoryService categoryService) {
        this.categoryService = categoryService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/")
    @PreAuthorize("isAnonymous()")
    public ModelAndView index() {
        return super.view("index");
    }

    @GetMapping("/home")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView home(ModelAndView modelAndView) {
        List<CategoryAllViewModel> allCategories = this.categoryService.findAllCategories()
                .stream()
                .map(categoryServiceModel -> this.modelMapper.map(categoryServiceModel, CategoryAllViewModel.class))
                .collect(Collectors.toList());
        modelAndView.addObject("categories", allCategories);
        return super.view("home", modelAndView);
    }
}