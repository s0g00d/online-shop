package com.online.shop.web.controllers;

import com.online.shop.domain.model.bindings.CategoryBindingModel;
import com.online.shop.domain.model.service.CategoryServiceModel;
import com.online.shop.domain.model.view.CategoryAllViewModel;
import com.online.shop.service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/categories")
public class CategoryController extends BaseController{

    private final ModelMapper mapper;
    private final CategoryService categoryService;

    public CategoryController(ModelMapper mapper, CategoryService categoryService) {
        this.mapper = mapper;
        this.categoryService = categoryService;
    }

    @GetMapping("/add")
    @PreAuthorize("hasRole('ROLE_MODERATOR')")
    public ModelAndView categoryAdd(ModelAndView modelAndView, @ModelAttribute(name = "model") CategoryBindingModel model){
        modelAndView.addObject("model", model);
        return this.view("category/add-category", modelAndView);
    }

    @PostMapping("/add")
    @PreAuthorize("hasRole('ROLE_MODERATOR')")
    public ModelAndView categoryAddConfirm(ModelAndView modelAndView, @ModelAttribute(name = "model") CategoryBindingModel model, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            modelAndView.addObject("model", model);
            return this.view("category/all-categories", modelAndView);
        }
        CategoryServiceModel categoryServiceModel = this.mapper.map(model, CategoryServiceModel.class);
        this.categoryService.addCategory(categoryServiceModel);
        modelAndView.addObject("model", model);
        return super.redirect("/categories/all");
    }

    @GetMapping("all")
    @PreAuthorize("hasRole('ROLE_MODERATOR')")
    public ModelAndView showAllCategories(ModelAndView modelAndView, @ModelAttribute(name="model") CategoryBindingModel model){
        List<CategoryAllViewModel> allCategories = this.categoryService.findAllCategories()
                .stream()
                .map(categoryServiceModel -> this.mapper.map(categoryServiceModel, CategoryAllViewModel.class))
                .collect(Collectors.toList());
        modelAndView.addObject("categories", allCategories);
        return super.view("category/all-categories", modelAndView);
    }

    @PostMapping("/delete")
    @PreAuthorize("hasRole('ROLE_MODERATOR')")
    public ModelAndView categoryDeleteConfirm(ModelAndView modelAndView, @ModelAttribute(name = "model") CategoryBindingModel model){

        CategoryServiceModel categoryServiceModel = this.mapper.map(model, CategoryServiceModel.class);
        this.categoryService.deleteCategory(categoryServiceModel);
        modelAndView.addObject("model", model);
        return this.redirect("category/all-categories");
    }

    @PostMapping("/edit/{id}")
    @PreAuthorize("hasRole('ROLE_MODERATOR')")
    public ModelAndView categoryEditConfirm(ModelAndView modelAndView, @PathVariable(name = "id") String id,@ModelAttribute(name = "model") CategoryBindingModel model, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            modelAndView.addObject("model", model);
            return this.view("category/all-categories", modelAndView);
        }
        CategoryServiceModel categoryServiceModel = this.mapper.map(model, CategoryServiceModel.class);
        this.categoryService.editCategory(categoryServiceModel);
        modelAndView.addObject("model", model);
        return super.redirect("/categories/all");
    }
    @GetMapping("/edit/{id}")
    @PreAuthorize("hasRole('ROLE_MODERATOR')")
    public ModelAndView categoryEdit(ModelAndView modelAndView, @PathVariable(name = "id") String id, @ModelAttribute(name = "model") CategoryBindingModel model){

        CategoryServiceModel categoryServiceModel = this.categoryService.findCategoryById(id);
        model = this.mapper.map(categoryServiceModel, CategoryBindingModel.class);

      //  modelAndView.addObject("categoryId", id);
        modelAndView.addObject("model", model);

        return super.view("category/edit-category", modelAndView);
    }

    @PostMapping("/delete/{id}")
    @PreAuthorize("hasRole('ROLE_MODERATOR')")
    public ModelAndView categoryDeleteConfirm(ModelAndView modelAndView, @PathVariable(name = "id") String id,@ModelAttribute(name = "model") CategoryBindingModel model, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            modelAndView.addObject("model", model);
            return this.view("category/all-categories", modelAndView);
        }
        CategoryServiceModel categoryServiceModel = this.mapper.map(model, CategoryServiceModel.class);
        this.categoryService.deleteCategory(categoryServiceModel);
        modelAndView.addObject("model", model);
        return super.redirect("/categories/all");
    }
    @GetMapping("/delete/{id}")
    @PreAuthorize("hasRole('ROLE_MODERATOR')")
    public ModelAndView categoryDelete(ModelAndView modelAndView, @PathVariable(name = "id") String id, @ModelAttribute(name = "model") CategoryBindingModel model){

        CategoryServiceModel categoryServiceModel = this.categoryService.findCategoryById(id);
        model = this.mapper.map(categoryServiceModel, CategoryBindingModel.class);

        //  modelAndView.addObject("categoryId", id);
        modelAndView.addObject("model", model);

        return super.view("category/delete-category", modelAndView);
    }

    @GetMapping("/fetch")
    @PreAuthorize("hasRole('ROLE_MODERATOR')")
    @ResponseBody
    public List<CategoryAllViewModel> fetchCategories() {
        List<CategoryAllViewModel> categories = this.categoryService.findAllCategories()
                .stream()
                .map(c -> this.mapper.map(c, CategoryAllViewModel.class))
                .collect(Collectors.toList());

        return categories;
    }
}
