package com.online.shop.web.controllers;

import com.online.shop.domain.model.rest.CartBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/cart")
public class CartApiController {

    @PostMapping("/details")
    public ResponseEntity<List<CartBean>> setCartDetails(@RequestBody List<CartBean> input, HttpSession session) throws Exception {

        List<CartBean> shopItems = new ArrayList<CartBean>();
        if(input.size() > 0) {
            for (int i = 0; i < input.size(); i++) {
                shopItems.add(input.get(i));
            }
        }

        session.setAttribute("shopping-cart", shopItems);
        return  new ResponseEntity<>(input, HttpStatus.OK);
    }
}
