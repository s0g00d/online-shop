package com.online.shop.service;

import com.online.shop.domain.model.service.ProductServiceModel;

import java.util.List;

public interface ProductService {

    ProductServiceModel addProduct(ProductServiceModel productServiceModel);

    ProductServiceModel editProduct(String id, ProductServiceModel productServiceModel);

    boolean deleteProduct(String id);

    ProductServiceModel findProductById(String id);

    List<ProductServiceModel> findAllProducts();

    List<ProductServiceModel> findAllByCategory(String category);

    ProductServiceModel findProductByName(String name);
}
