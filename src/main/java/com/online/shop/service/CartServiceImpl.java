package com.online.shop.service;

import com.online.shop.domain.enitities.Product;
import com.online.shop.domain.enitities.ShoppingCart;
import com.online.shop.domain.enitities.User;
import com.online.shop.domain.model.service.CartServiceModel;
import com.online.shop.error.ProductNotFoundException;
import com.online.shop.repositories.ProductRepository;
import com.online.shop.repositories.ShoppingCartRepository;
import com.online.shop.repositories.UserRepository;
import com.online.shop.domain.model.rest.CartBean;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CartServiceImpl implements CartService {

    private final ShoppingCartRepository shoppingCartRepository;
    private final ProductRepository productRepository;
    private final ModelMapper modelMapper;
    private final UserRepository userRepository;

    @Autowired
    public CartServiceImpl(ShoppingCartRepository shoppingCartRepository, ProductRepository productRepository, ModelMapper modelMapper, UserRepository userRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.productRepository = productRepository;
        this.modelMapper = modelMapper;
        this.userRepository = userRepository;
    }

    @Override
    public boolean removeItemFromCart(String id) {
        ShoppingCart shoppingCartItem = this.shoppingCartRepository.findById(id).orElseThrow(() -> new ProductNotFoundException("Product not deleted"));
        if(shoppingCartItem != null){
            try{
                this.shoppingCartRepository.delete(shoppingCartItem);
                return  true;
            }catch(ProductNotFoundException ex){
                return false;
            }
        }else{
            return false;
        }
    }

    @Override
    public void addItemsToCart(List<CartBean> items, String userName) {
        List<ShoppingCart> shoppingCartItems = new ArrayList<ShoppingCart>();
        User user = this.userRepository.findByUsername(userName).orElseThrow();
        String userId = user.getId();
        for (int i = 0; i < items.size(); i++){
            ShoppingCart shoppingCart = new ShoppingCart();
            Product product = productRepository.findByName(items.get(i).getName()).orElseThrow(() -> new ProductNotFoundException("Product not found!"));
            shoppingCart.setUserId(userId);
            shoppingCart.setName(product.getName());
            shoppingCart.setPrice(product.getPrice());
            shoppingCart.setQuantity(items.get(i).getQuantity());
            shoppingCartItems.add(shoppingCart);
        }
        this.shoppingCartRepository.saveAll(shoppingCartItems);
    }

    @Override
    public List<CartServiceModel> listAllCartItems(String name) {
        User user = this.userRepository.findByUsername(name).orElseThrow();
        String userId = user.getId();
        List<ShoppingCart> items = this.shoppingCartRepository.findAllByUserId(userId);
        return items
                .stream()
                .map(sc -> this.modelMapper.map(sc, CartServiceModel.class))
                .collect(Collectors.toList());
    }

    @Override
    public CartServiceModel findUserItemByName(String name, String userId) {
        ShoppingCart shoppingCart = this.shoppingCartRepository.findByNameAndUserId(name, userId)
                .orElseThrow();
        CartServiceModel cartServiceModel = this.modelMapper.map(shoppingCart, CartServiceModel.class);
        return cartServiceModel;
    }
}
