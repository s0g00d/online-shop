package com.online.shop.service;

import com.online.shop.domain.model.service.CategoryServiceModel;

import java.util.List;

public interface CategoryService {
    CategoryServiceModel addCategory(CategoryServiceModel categoryServiceModel);

    CategoryServiceModel findCategoryById(String id);

    CategoryServiceModel editCategory(CategoryServiceModel categoryServiceModel);

    boolean deleteCategory(CategoryServiceModel categoryServiceModel);

    List<CategoryServiceModel> findAllCategories();
}
