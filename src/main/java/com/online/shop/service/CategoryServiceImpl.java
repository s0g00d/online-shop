package com.online.shop.service;

import com.online.shop.domain.enitities.Category;
import com.online.shop.domain.model.service.CategoryServiceModel;
import com.online.shop.repositories.CategoryRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService{

    private final CategoryRepository categoryRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository, ModelMapper modelMapper) {
        this.categoryRepository = categoryRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public CategoryServiceModel addCategory(CategoryServiceModel categoryServiceModel) {
        /*if (!validator.validate(categoryServiceModel).isEmpty()) {
            throw new IllegalArgumentException("Invalid Category");
        }*/

        Category category = this.modelMapper.map(categoryServiceModel, Category.class);
        category = this.categoryRepository.saveAndFlush(category);
        return this.modelMapper.map(category, CategoryServiceModel.class);
    }

    @Override
    public CategoryServiceModel findCategoryById(String id) {
        if(id.isEmpty()){
            throw new IllegalArgumentException("Category not exist");
        }
        Category category = this.categoryRepository.findById(id).orElse(null);
        if(category == null){
            throw new IllegalArgumentException("Category not found");
        }
        return this.modelMapper.map(category, CategoryServiceModel.class);
    }

    @Override
    public CategoryServiceModel editCategory(CategoryServiceModel categoryServiceModel) {
       /* if (!validator.validate(categoryServiceModel).isEmpty()) {
            throw new IllegalArgumentException("Invalid Category");
        }*/
        Category category = this.modelMapper.map(categoryServiceModel,Category.class);
        category.setName(categoryServiceModel.getName());
        category = this.categoryRepository.saveAndFlush(category);
        return this.modelMapper.map(category, CategoryServiceModel.class);
    }

    @Override
    public boolean deleteCategory(CategoryServiceModel categoryServiceModel) {
       /* if (!validator.validate(categoryServiceModel).isEmpty()) {
            throw new IllegalArgumentException("Invalid Category");
        }*/
        Category category = this.modelMapper.map(categoryServiceModel,Category.class);
        try{
            this.categoryRepository.delete(category);
        }catch(Exception e){
            return false;
        }
        return true;
    }

    @Override
    public List<CategoryServiceModel> findAllCategories() {
        return this.categoryRepository.findAll()
                .stream()
                .map(category -> this.modelMapper.map(category, CategoryServiceModel.class))
                .collect(Collectors.toList());
    }
}
