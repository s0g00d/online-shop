package com.online.shop.service;

import com.online.shop.domain.model.service.CartServiceModel;
import com.online.shop.domain.model.rest.CartBean;

import java.util.List;

public interface CartService {

    public boolean removeItemFromCart(String id);

    public void addItemsToCart(List<CartBean> items, String name);

    public List<CartServiceModel> listAllCartItems(String userId);

    public CartServiceModel findUserItemByName(String name, String userId);

}
