package com.online.shop.service;

import com.online.shop.domain.model.service.RoleServiceModel;

import java.util.Set;

public interface RoleService {

    void seedRolesInDb();
    Set<RoleServiceModel> findAllRoles();

    RoleServiceModel findByAuthority(String authority);
}
