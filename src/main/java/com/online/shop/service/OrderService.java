package com.online.shop.service;

import com.online.shop.domain.model.service.OrderServiceModel;

import java.util.List;

public interface OrderService {
    public void createOrder(OrderServiceModel orderServiceModel);
    public List<OrderServiceModel> findAllOrders();
    public List<OrderServiceModel> findOrdersByCustomer(String name);
    public OrderServiceModel findOrderById(String id);
}
