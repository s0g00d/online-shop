package com.online.shop.domain.enitities;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name="shopping_cart")
public class ShoppingCart extends BaseEntity{

    private String userId;
    private String name;
    private BigDecimal price;
    private int quantity;

    @Column(name="item_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="item_price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Column(name="quantity")
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Column(name="user_id")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
