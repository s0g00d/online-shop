package com.online.shop.domain.model.service;

public class CategoryServiceModel extends BaseServiceModel {

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
