package com.online.shop.domain.model.rest;

public class CartBean {

    private String name;
    private int quantity;

    public CartBean(String name, int quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    public CartBean() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
