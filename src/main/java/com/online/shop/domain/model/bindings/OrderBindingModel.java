package com.online.shop.domain.model.bindings;

import com.online.shop.domain.enitities.User;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class OrderBindingModel extends BaseBindingModel{
    private List<String> orderProducts;
    private User customer;
    private LocalDateTime orderDate;
    private BigDecimal totalPrice;

    public List<String> getOrderProducts() {
        return orderProducts;
    }

    public void setOrderProducts(List<String> orderProducts) {
        this.orderProducts = orderProducts;
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }
}
