package com.online.shop.domain.model.bindings;

public abstract class BaseBindingModel {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
