package com.online.shop.repositories;

import com.online.shop.domain.enitities.ShoppingCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, String> {
    Optional<ShoppingCart> findById(String id);
    List<ShoppingCart> findAllByUserId(String userId);
    Optional<ShoppingCart> findByNameAndUserId(String name, String userId);
}
