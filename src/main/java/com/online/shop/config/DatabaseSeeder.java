package com.online.shop.config;

import com.online.shop.domain.enitities.Role;
import com.online.shop.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DatabaseSeeder {
    private final RoleRepository userRoleRepository;

    @Autowired
    public  DatabaseSeeder(RoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    @PostConstruct
    public void seed() {
        if (this.userRoleRepository.findAll().isEmpty()) {
            Role adminRole = new Role();
            adminRole.setAuthority("ROLE_ADMIN");

            Role userRole = new Role();
            userRole.setAuthority("ROLE_USER");

            Role moderatorRole = new Role();
            moderatorRole.setAuthority("ROLE_MODERATOR");

            this.userRoleRepository.saveAndFlush(adminRole);
            this.userRoleRepository.saveAndFlush(userRole);
            this.userRoleRepository.saveAndFlush(moderatorRole);
        }
    }
}
