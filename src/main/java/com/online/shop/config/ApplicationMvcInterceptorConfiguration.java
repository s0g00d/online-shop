package com.online.shop.config;

import com.online.shop.web.interceptors.ShopItemsInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ApplicationMvcInterceptorConfiguration implements WebMvcConfigurer {

    private final ShopItemsInterceptor shopItemsInterceptor;

    public ApplicationMvcInterceptorConfiguration(ShopItemsInterceptor shopItemsInterceptor) {
        this.shopItemsInterceptor = shopItemsInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this.shopItemsInterceptor);

    }
}
